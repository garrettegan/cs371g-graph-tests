// -------------
// TestGraph.c++
// -------------

// https://www.boost.org/doc/libs/1_73_0/libs/graph/doc/index.html

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <iterator> // ostream_iterator
#include <sstream>  // ostringstream
#include <utility>  // pair

#include "boost/graph/adjacency_list.hpp" // adjacency_list

#include "gtest/gtest.h"

#include "Graph.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// ---------
// TestGraph
// ---------

template <typename G>
struct GraphFixture : Test {
    // ------
    // usings
    // ------

    using graph_type          = G;
    using vertex_descriptor   = typename G::vertex_descriptor;
    using edge_descriptor     = typename G::edge_descriptor;
    using vertex_iterator     = typename G::vertex_iterator;
    using edge_iterator       = typename G::edge_iterator;
    using adjacency_iterator  = typename G::adjacency_iterator;
    using vertices_size_type  = typename G::vertices_size_type;
    using edges_size_type     = typename G::edges_size_type;};

// directed, sparse, unweighted
// possibly connected
// possibly cyclic
using
    graph_types =
    Types<
            boost::adjacency_list<boost::setS, boost::vecS, boost::directedS>,
         Graph // uncomment
            >;

#ifdef __APPLE__
    TYPED_TEST_CASE(GraphFixture, graph_types,);
#else
    TYPED_TEST_CASE(GraphFixture, graph_types);
#endif


// --------
// provided
// --------

TYPED_TEST(GraphFixture, test0) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    vertex_descriptor vd = vertex(0, g);
    ASSERT_EQ(vd, vdA);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 1u);}

TYPED_TEST(GraphFixture, test1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
    ASSERT_EQ(p1.first,  edAB);
    ASSERT_EQ(p1.second, false);

    pair<edge_descriptor, bool> p2 = edge(vdA, vdB, g);
    ASSERT_EQ(p2.first,  edAB);
    ASSERT_EQ(p2.second, true);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);

    vertex_descriptor vd1 = source(edAB, g);
    ASSERT_EQ(vd1, vdA);

    vertex_descriptor vd2 = target(edAB, g);
    ASSERT_EQ(vd2, vdB);}

TYPED_TEST(GraphFixture, test2) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdA);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdB);
    ++b;
    ASSERT_EQ(b, e);}

TYPED_TEST(GraphFixture, test3) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_NE(b, e);

    set<edge_descriptor> resultSet;
    edge_descriptor ed1 = *b;
    resultSet.insert(ed1);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed2 = *b;
    resultSet.insert(ed2);
    ++b;
    ASSERT_EQ(e, b);
    
    ASSERT_EQ(resultSet, set<edge_descriptor>({edAB, edAC}));}

TYPED_TEST(GraphFixture, test4) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    set<vertex_descriptor> resultSet;
    vertex_descriptor vd1 = *b;
    resultSet.insert(vd1);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    resultSet.insert(vd2);
    ++b;
    ASSERT_EQ(e, b);
    
    ASSERT_EQ(resultSet, set<vertex_descriptor>({vdB, vdC}));}

// -------
// addEdge
// -------

TYPED_TEST(GraphFixture, addEdgeTRUE) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    pair<edge_descriptor, bool> edge = add_edge(v1, v2, g);
    ASSERT_TRUE(edge.second);}

TYPED_TEST(GraphFixture, addEdgeFalse) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    add_edge(v1, v2, g);
    pair<edge_descriptor, bool> edge2 = add_edge(v1, v2, g);
    ASSERT_FALSE(edge2.second);}

TYPED_TEST(GraphFixture, addEdgeAddVerticesToFit) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    pair<edge_descriptor, bool> edge1 = add_edge(v1, v2, g);
    ASSERT_TRUE(edge1.second);
    pair<edge_descriptor, bool> edge2 = add_edge(v1, 99, g);
    ASSERT_TRUE(edge2.second);
    ASSERT_EQ(num_vertices(g), 100u);}

// -----------
// numVertices
// -----------

TYPED_TEST(GraphFixture, numVerticesNone) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    ASSERT_EQ(0u, num_vertices(g));}

TYPED_TEST(GraphFixture, numVertices1) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;
    add_vertex(g);

    ASSERT_EQ(1u, num_vertices(g));}

TYPED_TEST(GraphFixture, numVertices2) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;
    add_vertex(g);
    add_vertex(g);

    ASSERT_EQ(2u, num_vertices(g));}

// --------
// numEdges
// --------

TYPED_TEST(GraphFixture, numEdgesNone) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    ASSERT_EQ(0u, num_edges(g));}

TYPED_TEST(GraphFixture, numEdgesNoneWithVertices) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;
    add_vertex(g);
    add_vertex(g);

    ASSERT_EQ(0u, num_edges(g));}

TYPED_TEST(GraphFixture, numEdges1) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    add_edge(v1, v2, g);

    ASSERT_EQ(1u, num_edges(g));}

TYPED_TEST(GraphFixture, numEdges2) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    add_edge(v1, v2, g);
    add_edge(v2, v1, g);

    ASSERT_EQ(2u, num_edges(g));}

// ----
// edge
// ----

TYPED_TEST(GraphFixture, edgeFalse) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);

    ASSERT_FALSE(edge(v1, v2, g).second);}

TYPED_TEST(GraphFixture, edgeFalseReversed) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    add_edge(v1, v2, g);

    ASSERT_FALSE(edge(v2, v1, g).second);}

TYPED_TEST(GraphFixture, edgeFalseIndirect) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    vertex_descriptor v3 = add_vertex(g);
    add_edge(v1, v2, g);
    add_edge(v2, v3, g);

    ASSERT_FALSE(edge(v1, v3, g).second);}

TYPED_TEST(GraphFixture, edgeTrue) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    add_edge(v1, v2, g);

    ASSERT_TRUE(edge(v1, v2, g).second);}

// --------
// vertices
// --------

TYPED_TEST(GraphFixture, verticesNone) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_iterator  = typename TestFixture::vertex_iterator;

    graph_type g;

    pair<vertex_iterator, vertex_iterator> vertexPair = vertices(g);
    ASSERT_EQ(vertexPair.first, vertexPair.second);}

TYPED_TEST(GraphFixture, vertices1) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_iterator  = typename TestFixture::vertex_iterator;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);

    pair<vertex_iterator, vertex_iterator> vertexPair = vertices(g);
    vertex_iterator b = vertexPair.first;
    vertex_iterator e = vertexPair.second;

    ASSERT_EQ(*b, v1);
    ++b;

    ASSERT_EQ(b, e);}

TYPED_TEST(GraphFixture, verticesMultiple) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_iterator  = typename TestFixture::vertex_iterator;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    vertex_descriptor v3 = add_vertex(g);
    vertex_descriptor v4 = add_vertex(g);

    pair<vertex_iterator, vertex_iterator> vertexPair = vertices(g);
    vertex_iterator b = vertexPair.first;
    vertex_iterator e = vertexPair.second;

    ASSERT_EQ(*b, v1);
    ++b;
    ASSERT_EQ(*b, v2);
    ++b;
    ASSERT_EQ(*b, v3);
    ++b;
    ASSERT_EQ(*b, v4);
    ++b;

    ASSERT_EQ(b, e);}

// -----------------
// adjacent_vertices
// -----------------

TYPED_TEST(GraphFixture, adjacentVerticesOneVertex) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);

    pair<adjacency_iterator, adjacency_iterator> adjPair = adjacent_vertices(v1, g);
    ASSERT_EQ(adjPair.first, adjPair.second);}

TYPED_TEST(GraphFixture, adjacentVerticesNoEdges) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    add_vertex(g);

    pair<adjacency_iterator, adjacency_iterator> adjPair = adjacent_vertices(v1, g);
    ASSERT_EQ(adjPair.first, adjPair.second);}

TYPED_TEST(GraphFixture, adjacentVerticesNone) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    vertex_descriptor v3 = add_vertex(g);
    add_edge(v2, v3, g);

    pair<adjacency_iterator, adjacency_iterator> adjPair = adjacent_vertices(v1, g);
    ASSERT_EQ(adjPair.first, adjPair.second);}

TYPED_TEST(GraphFixture, adjacentVerticesLoneEdge) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor  = typename TestFixture::edge_descriptor;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    pair<edge_descriptor, bool> edgePair = add_edge(v1, v2, g);
    ASSERT_TRUE(edgePair.second);

    pair<adjacency_iterator, adjacency_iterator> adjPair = adjacent_vertices(v1, g);
    adjacency_iterator b = adjPair.first;
    adjacency_iterator e = adjPair.second;

    ASSERT_EQ(*b, v2);
    ++b;
    ASSERT_EQ(b, e);}

TYPED_TEST(GraphFixture, adjacentVerticesMultipleEdges) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor  = typename TestFixture::edge_descriptor;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    vertex_descriptor v3 = add_vertex(g);
    vertex_descriptor v4 = add_vertex(g);
    pair<edge_descriptor, bool> edgePair1 = add_edge(v1, v2, g);
    ASSERT_TRUE(edgePair1.second);
    pair<edge_descriptor, bool> edgePair2 = add_edge(v1, v3, g);
    ASSERT_TRUE(edgePair2.second);
    pair<edge_descriptor, bool> edgePair3 = add_edge(v1, v4, g);
    ASSERT_TRUE(edgePair3.second);

    pair<adjacency_iterator, adjacency_iterator> adjPair = adjacent_vertices(v1, g);
    adjacency_iterator b = adjPair.first;
    adjacency_iterator e = adjPair.second;

    unordered_set<vertex_descriptor> resultSet;
    resultSet.insert(*b);
    ++b;
    resultSet.insert(*b);
    ++b;
    resultSet.insert(*b);
    ++b;
    
    ASSERT_EQ(b, e);

    ASSERT_EQ(resultSet, unordered_set<vertex_descriptor>({v2, v3, v4}));}

TYPED_TEST(GraphFixture, adjacentVerticesMultipleEdgesMultipleSources) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor  = typename TestFixture::edge_descriptor;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    vertex_descriptor v3 = add_vertex(g);
    vertex_descriptor v4 = add_vertex(g);
    pair<edge_descriptor, bool> edgePair1 = add_edge(v1, v2, g);
    ASSERT_TRUE(edgePair1.second);
    pair<edge_descriptor, bool> edgePair2 = add_edge(v1, v3, g);
    ASSERT_TRUE(edgePair2.second);
    pair<edge_descriptor, bool> edgePair3 = add_edge(v1, v4, g);
    ASSERT_TRUE(edgePair3.second);
    pair<edge_descriptor, bool> edgePair4 = add_edge(v2, v4, g);
    ASSERT_TRUE(edgePair4.second);

    pair<adjacency_iterator, adjacency_iterator> adjPair = adjacent_vertices(v2, g);
    adjacency_iterator b = adjPair.first;
    adjacency_iterator e = adjPair.second;

    ASSERT_EQ(*b, v4);
    ++b;
    ASSERT_EQ(b, e);}

// -----
// edges
// -----

TYPED_TEST(GraphFixture, edgesNoVertices) {
    using graph_type          = typename TestFixture::graph_type;
    using edge_iterator  = typename TestFixture::edge_iterator;

    graph_type g;

    pair<edge_iterator, edge_iterator> edgesPair = edges(g);
    ASSERT_EQ(edgesPair.first, edgesPair.second);}

TYPED_TEST(GraphFixture, edgesOneVertex) {
    using graph_type          = typename TestFixture::graph_type;
    using edge_iterator  = typename TestFixture::edge_iterator;

    graph_type g;
    add_vertex(g);

    pair<edge_iterator, edge_iterator> edgesPair = edges(g);
    ASSERT_EQ(edgesPair.first, edgesPair.second);}

TYPED_TEST(GraphFixture, edgesNoEdges) {
    using graph_type          = typename TestFixture::graph_type;
    using edge_iterator  = typename TestFixture::edge_iterator;

    graph_type g;
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);

    pair<edge_iterator, edge_iterator> edgesPair = edges(g);
    ASSERT_EQ(edgesPair.first, edgesPair.second);}

TYPED_TEST(GraphFixture, edgesLoneEdge) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_iterator  = typename TestFixture::edge_iterator;
    using edge_descriptor  = typename TestFixture::edge_descriptor;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    pair<edge_descriptor, bool> edgePair = add_edge(v1, v2, g);
    ASSERT_TRUE(edgePair.second);

    pair<edge_iterator, edge_iterator> edgesPair = edges(g);
    edge_iterator b = edgesPair.first;
    edge_iterator e = edgesPair.second;

    ASSERT_EQ(*b, edgePair.first);
    ++b;
    ASSERT_EQ(b, e);}

TYPED_TEST(GraphFixture, edgesMultipleEdges) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_iterator  = typename TestFixture::edge_iterator;
    using edge_descriptor  = typename TestFixture::edge_descriptor;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    vertex_descriptor v3 = add_vertex(g);
    vertex_descriptor v4 = add_vertex(g);
    pair<edge_descriptor, bool> edgePair1 = add_edge(v1, v2, g);
    ASSERT_TRUE(edgePair1.second);
    pair<edge_descriptor, bool> edgePair2 = add_edge(v1, v3, g);
    ASSERT_TRUE(edgePair2.second);
    pair<edge_descriptor, bool> edgePair3 = add_edge(v1, v4, g);
    ASSERT_TRUE(edgePair3.second);

    pair<edge_iterator, edge_iterator> edgesPair = edges(g);
    edge_iterator b = edgesPair.first;
    edge_iterator e = edgesPair.second;

    set<edge_descriptor> resultSet;
    resultSet.insert(*b);
    ++b;
    resultSet.insert(*b);
    ++b;
    resultSet.insert(*b);
    ++b;
    ASSERT_EQ(b, e);

    ASSERT_EQ(resultSet, set<edge_descriptor>({edgePair1.first, edgePair2.first, edgePair3.first}));}

TYPED_TEST(GraphFixture, edgesMultipleEdgesMultipleSources) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_iterator  = typename TestFixture::edge_iterator;
    using edge_descriptor  = typename TestFixture::edge_descriptor;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    vertex_descriptor v3 = add_vertex(g);
    vertex_descriptor v4 = add_vertex(g);
    pair<edge_descriptor, bool> edgePair1 = add_edge(v1, v2, g);
    ASSERT_TRUE(edgePair1.second);
    pair<edge_descriptor, bool> edgePair2 = add_edge(v1, v3, g);
    ASSERT_TRUE(edgePair2.second);
    pair<edge_descriptor, bool> edgePair3 = add_edge(v1, v4, g);
    ASSERT_TRUE(edgePair3.second);
    pair<edge_descriptor, bool> edgePair4 = add_edge(v2, v4, g);
    ASSERT_TRUE(edgePair4.second);

    pair<edge_iterator, edge_iterator> edgesPair = edges(g);
    edge_iterator b = edgesPair.first;
    edge_iterator e = edgesPair.second;

    set<edge_descriptor> resultSet;
    resultSet.insert(*b);
    ++b;
    resultSet.insert(*b);
    ++b;
    resultSet.insert(*b);
    ++b;
    resultSet.insert(*b);
    ++b;
    ASSERT_EQ(b, e);

    ASSERT_EQ(resultSet, set<edge_descriptor>({edgePair1.first, edgePair2.first, edgePair3.first, edgePair4.first}));}

// ------
// source
// ------

TYPED_TEST(GraphFixture, source) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using edge_descriptor  = typename TestFixture::edge_descriptor;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    edge_descriptor edge = add_edge(v1, v2, g).first;
    ASSERT_EQ(source(edge, g), v1);}

TYPED_TEST(GraphFixture, sourceReverse) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using edge_descriptor  = typename TestFixture::edge_descriptor;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    edge_descriptor edge = add_edge(v2, v1, g).first;
    ASSERT_EQ(source(edge, g), v2);}

// ------
// target
// ------

TYPED_TEST(GraphFixture, target) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using edge_descriptor  = typename TestFixture::edge_descriptor;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    edge_descriptor edge = add_edge(v1, v2, g).first;
    ASSERT_EQ(target(edge, g), v2);}

TYPED_TEST(GraphFixture, targetReverse) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using edge_descriptor  = typename TestFixture::edge_descriptor;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    edge_descriptor edge = add_edge(v2, v1, g).first;
    ASSERT_EQ(target(edge, g), v1);}

// ------
// vertex
// ------

TYPED_TEST(GraphFixture, vertexDNE) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;

    graph_type g;
    
    vertex_descriptor v = vertex(5, g);
    ASSERT_EQ(v, 5u);
    ASSERT_EQ(num_vertices(g), 0);}

TYPED_TEST(GraphFixture, vertexExists) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;

    graph_type g;
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    
    vertex_descriptor v = vertex(5, g);
    ASSERT_EQ(v, 5u);
    ASSERT_EQ(num_vertices(g), 6u);}

// ----
// edge
// ----

TYPED_TEST(GraphFixture, edgeDNE) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using edge_descriptor  = typename TestFixture::edge_descriptor;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);

    pair<edge_descriptor, bool> edgePair = edge(v1, v2, g);
    
    ASSERT_FALSE(edgePair.second);
    ASSERT_EQ(num_edges(g), 0);}

TYPED_TEST(GraphFixture, edgeExists) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using edge_descriptor  = typename TestFixture::edge_descriptor;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    add_edge(v1, v2, g);

    pair<edge_descriptor, bool> edgePair = edge(v1, v2, g);
    
    ASSERT_TRUE(edgePair.second);
    ASSERT_EQ(num_edges(g), 1u);}